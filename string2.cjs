function string2(testString) {
    const regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;

    if(!regex.test(testString)) {
        return [];
    } else {
        let store = testString.split(".").map((index) => Number(index));
        return store;
    }
}

module.exports = string2;
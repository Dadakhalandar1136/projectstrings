function string3(testString) {
    let monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
    if(testString === undefined || testString.length === 0 ) {
        return [];
    } else {
        let splitStrore = testString.split('/');
        if(splitStrore.length == 1) {
            return [];
        } else {
            return monthNames[splitStrore[1]-1];
        }
    }
}

module.exports = string3;
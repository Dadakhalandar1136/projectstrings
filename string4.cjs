function string4(testString) {
    let stringArr = [];
    
    if(testString == undefined || testString.length == 0 ) {
        return [];
    }  
    for(let strValue in testString) {
        stringArr.push(testString[strValue]);    
    }

    let convertString = stringArr.toString().split(",").join(" ").toLowerCase().split(' ');

    for(let index = 0; index < convertString.length; index++) {
        convertString[index] = convertString[index].charAt(0).toUpperCase() + convertString[index].slice(1);
    }
    return convertString.join(' ');
}

module.exports = string4;
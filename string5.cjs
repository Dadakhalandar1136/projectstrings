function string5(testString) {

    if(testString == undefined || testString.length == 0 || !isNaN(testString)) {
        return [];
    } else {
        let store = testString.toString().split(",").join(" ");
        return store;
    }
}

module.exports = string5;
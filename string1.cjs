function string1(testString) {
    
    if (testString == undefined || testString.length == 0) {
        return 0;
    }
    let toString = testString.replaceAll(/[$,]/g, '');
    if (isNaN(toString)) {
        return 0;
    } else {
        return parseFloat(toString);
    }
}

module.exports = string1;